<?php

require("animal.php");
require("Frog.php");
require("Ape.php");

// release 0
$sheep = new Animal("shaun");

echo "Name : " .$sheep->name. "<br>"; // "shaun"
echo "legs : " .$sheep->legs ."<br>"; // 4
echo "cold blooded : ".$sheep->cold_blooded ."<br> <br>"; // "no"

// release 1

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "Name : " .$kodok->name. "<br>"; 
echo "legs : " .$kodok->legs ."<br>"; 
echo "cold blooded : ".$kodok->cold_blooded ."<br>"; 
echo "Jump : ".$kodok->jump(). "<br> <br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "Name : " .$sungokong->name. "<br>"; 
echo "legs : " .$sungokong->legs ."<br>"; 
echo "cold blooded : ".$sungokong->cold_blooded ."<br>";
echo "Yell : ". $sungokong->yell() ."<br> <br>";

?>